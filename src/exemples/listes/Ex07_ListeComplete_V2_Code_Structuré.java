package exemples.listes;

import static donnees.Club.listeDesPersonnes;
import donnees.Personne;
import static utilitaires.UtilDate.*;
import static utilitaires.UtilDojo.*;

public class Ex07_ListeComplete_V2_Code_Structuré {

    public static void main(String[] args) {
        
        afficherTitre();
         
        for(Personne pers : listeDesPersonnes){    
           
            afficherPersonne(pers);
        }
        
        System.out.println();       
    }

    private static void afficherPersonne(Personne pPers) {
        
        System.out.printf(
                           "%-15s %-15s né(e) le: %-10s %2d ans poids %4d kg catégorie: %-20s\n",
                           
                           pPers.prenom, 
                           pPers.nom,
                           dateVersChaine(pPers.dateNaiss),
                           ageEnAnnees(pPers.dateNaiss),
                           pPers.poids,
                           determineCategorie( pPers.sexe, pPers.poids )  
                         );
    }

    private static void afficherTitre() {
        
        System.out.printf("\nComposition du club de Judo au: %10s \n\n",aujourdhuiChaine());
    }
  
}
