
package exemples.tableassoc;

import java.util.Map;
import java.util.TreeMap;


public class Ex08_TableauAssociatif_AccesDirect {

    public static void main(String[] args) {
       
        // Déclaration du tableau associatif  resultat la clé est de type String et la valeur de type Integer
        // Les entrées du tableau seront rangés dans l'ordre croissant de la clé ( TreeMap)
        // A chaque clé est associé une valeur 
        // C'est à dire ici qu'à chaque nom on associe une note  
        
        Map<String,Integer> resultats=new TreeMap();
        
        
        // Enregistrement des entrées dans le tableau associatif avec la fonction put
        // qui prend deux paramètres: la clé et la valeur associée à cette clé 
        
        resultats.put("Pierre", 12);
        resultats.put("Alain", 10);
        resultats.put("Jacques", 8);
        resultats.put("Fabienne", 16);
        resultats.put("Marie", 13);
        resultats.put("Béatrice", 14);
        resultats.put("Léa", 15);
        resultats.put("Thierry", 16);
        
        // Accès direct à l'entrée correspondant à  de Léa et affichage
        // La clé est le nom de type String
        // La valeur est la note de type Integer
        
       
        System.out.println();
        System.out.println("Léa a obtenu:  "+resultats.get("Léa"));
        System.out.println();
       
        //  Accès direct à l'entrée corrspondant à  la clé  Léa et 
        // Modification de  la valeur associée c'est à dire la note
        
        System.out.println("On monte la note de Léa à 17");
        resultats.put("Léa", 17);
        
        // Accès direct à la note de Léa et affichage
        
        System.out.println("Léa a maintenant:  "+resultats.get("Léa"));
        
    }
}
