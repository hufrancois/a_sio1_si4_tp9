
package exemples.tableassoc;

import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class Ex10_TableauAssociatif_RepartitionV2 {

    public static void main(String[] args) {
       
        // Déclaration et initialisation 
        // d'un tableau d'entiers à une dimension.
        Integer[ ] notes={
                           12, 4, 8,15,12,10,2, 8,14,13,12,12,14,
                           15,16, 2,11,10,13,16,12,11,19,13, 7,11,
                           20,12,13,14,15, 9, 3, 9,10, 3,16,13,19,
                           13, 8,15, 7, 8,11,18,10, 9,14,15,12,10,
                           15, 8, 6, 4,10,0,16, 8, 4,18,13, 7,12
                         };
        
        // On déclare le tableau associatif repart
        Map<Integer, Integer> repart= new TreeMap();
                
        // On parcourt le tableau note 
        // chaque note rencontré n 
        // sera une clé dans le tableau repart
        for( Integer n : notes){
       
           // val contiendra la valeur à stoker
           // dans le tableau associatif repart 
           Integer val; 
            
           // si la clé n existe déjà dans repart
           // on le sait avec la syntaxe: repart.containsKey(n)
           
           if( repart.containsKey(n) ) {
              
              // La clé est déjà dans le tableau repart
              // on ajoute 1  à la valeur stockée
              // pour la clé n
              // et on l'affecte à val 
               
              val=repart.get(n)+1;
           }
           else {
              
             // si la clé n'est pas déjà dans le tableau
             // associatif on affecte la valeur 1 à val  
               
              val=1;
           }
           
           // on range la valeur stockée dans val
           // dans l'entrée de clé n du tablaeu associatif
           // si la clé n'existait pas elle sera créée
           // avec la valeur 1 associée
           repart.put(n, val);
        }
        
        System.out.println("Répartition des notes contenues dans le tableau notes\n");
        
        
        // On affiche les entrées du tableau repart
        // Il n'y a pas d'entrée à laquelle la valeur 0 
        // serait associée
        
        for( Entry  e: repart.entrySet()){
        
            System.out.printf("Nombre de %3d: %3d\n",e.getKey(),e.getValue() );
        }
        System.out.println();
       
    }
}
